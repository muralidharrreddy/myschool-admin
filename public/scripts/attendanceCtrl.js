(function () {
    'use strict';
    angular.module('authApp').controller('attendanceCtrl', function ($scope, $http, $auth, $rootScope, SweetAlert) {
        $rootScope.isLogin = true;
        $rootScope.data.showPreviewBtn = false;
        $scope.items = [];
        $scope.table  = [];
        $scope.attendanceData={};
        $scope.pasteEnable=false;
        $scope.isAttendanceData=false;
        $scope.$watch('items', function (newValue, oldValue, scope) {
                $scope.createTable(newValue);
        });
        $scope.submit=function(data){
            var fileName=data.fileName;
            var UID=data.UID;
            delete data.fileName;
            delete data.UID;
            if($scope.isAttendanceDataEdit){
                angular.forEach(data, function(value1, key1) {
                    angular.forEach( $scope.attendanceDataEdit, function(value2, key2) {
                        if (value1['adNo'] === value2['adNo'] && value1['class'] === value2['class']
                            &&value1['section'] === value2['section'] && value1['rollNo'] === value2['rollNo']
                            &&value1['date'] === value2['date'] && value1['status'] === value2['status']
                        ) {
                          delete data[key1];
                        }
                    });
                });
                $http.put('api/attendanceData/putAttendanceData',{'data':data,'fileName':fileName,'UID':UID})
                    .success(function(data){
                        $scope.table  = [];
                        $scope.items = [];
                        $scope.isAttendanceData=false;
                        SweetAlert.swal("Success", "Attendance Upload Completed", "success");
                        $scope.getPrevAttendanceData();
                    })
                    .error(function(res){
                        $scope.table  = [];
                        $scope.items = [];
                        $scope.isAttendanceData=false;
                    });
            }
            else{
                $http.post('api/attendanceData/postAttendanceData',{'data':data,'fileName':fileName})
                    .success(function(data){
                        $scope.table  = [];
                        $scope.items = [];
                        $scope.isAttendanceData=false;
                        SweetAlert.swal("Success", "Attendance Upload Completed", "success");
                        $scope.getPrevAttendanceData();
                    })
                    .error(function(res){
                        $scope.table  = [];
                        $scope.items = [];
                        $scope.isAttendanceData=false;
                    });
            }

        };
        $scope.getPrevAttendanceData=function(){
            $http.get('api/attendanceData/getAttendanceData')
                .success(function(data){
                    $scope.attendanceDataPrev=data.attendanceData;
                })
                .error(function(res){

                });
        };
        $scope.getPrevAttendanceData();
        $scope.editAttendance=function(atten){
            $http.get('api/attendanceData/getAttendanceDataByUID?UID='+atten.UID)
                .success(function(data){
                    $scope.isAttendanceData=true;
                    $scope.isAttendanceDataEdit=true;
                    $scope.table=data.attendanceData;
                    $scope.attendanceDataEdit=data.attendanceData;
                    $scope.attendanceData.fileName=atten.name;
                    $scope.attendanceData.UID=atten.UID;
                })
                .error(function(res){

                });
        };
        $scope.deleteAttendance=function(id){
            SweetAlert.swal({   title: "Are you sure?",
                    text: "You will not be able to recover this data !",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: true },
                function(isConfirm){
                    if (isConfirm) {
                        $http.delete('api/attendanceData/deleteAttendanceDataByUID?UID='+id)
                            .success(function(data){
                                $scope.getPrevAttendanceData();
                            })
                            .error(function(res){

                            });
                    } else {

                    }


                });

        };
        $scope.createTable=function(d){
            $scope.table  =[];
            angular.forEach(d,function(data,key){
                var ss=null;
                if(data.indexOf(',') > -1){
                    ss=data.split(',');
                    //$rootScope.data.showPreviewBtn = true;
                    var item={};
                    item["adNo"]=ss[0];
                    item["class"]=ss[1];
                    item["section"]=ss[2];
                    item["rollNo"]=ss[3];
                    item["date"]=ss[4];
                    item["status"]=ss[5];
                    $scope.table.push(item);
                    $scope.isAttendanceData=true;
                    $scope.isAttendanceDataEdit=false;
                }
                else  if(data.indexOf('\t') > -1) {
                    ss = data.split('\t');
                    //$rootScope.data.showPreviewBtn = true;
                    var item={};
                    item["adNo"]=ss[0];
                    item["class"]=ss[1];
                    item["section"]=ss[2];
                    item["rollNo"]=ss[3];
                    item["date"]=ss[4];
                    item["status"]=ss[5];
                    $scope.table.push(item);
                    $scope.isAttendanceData=true;
                    $scope.isAttendanceDataEdit=false;
                }
                else{
                    SweetAlert.swal("Error", "Data Is not correct format.Copy data from excel and paste in input box.", "error");
                }
            });
        };
        $scope.clearData=function(){
            SweetAlert.swal({   title: "Are you sure?",
                    text: "You will not be able to recover this field !",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, delete it!",
                    closeOnConfirm: true },
                function(isConfirm){
                    if (isConfirm) {
                        $scope.table  = [];
                        $scope.items = [];
                        $scope.isAttendanceData=false;
                    } else {

                    }


                });
        };
        function isValidDate(dateString) {
            var regEx = /^\d{4}-\d{2}-\d{2}$/;
            if (!dateString.match(regEx))
                return false;  // Invalid format
            var d;
            if (!((d = new Date(dateString)) | 0))
                return false; // Invalid date (or this could be epoch)
            return d.toISOString().slice(0, 10) == dateString;
        }
    })
})();