(function() {

    'use strict';

    angular
        .module('authApp')
        .controller('resultsCtrl', function ($http, $auth, $rootScope) {
            $rootScope.isLogin=true;
            $rootScope.data.showPreviewBtn = false;
            function AppController(Papa) {

                function handleParseResult(result) {
                    // do something useful with the parsed data
                }

                function handleParseError(result) {
                    // display error message to the user
                }

                function parsingFinished() {
                    // whatever needs to be done after the parsing has finished
                }

                function parseCSV(data) {
                    Papa.parse(data)
                        .then(handleParseResult)
                        .catch(handleParseError)
                        .finally(parsingFinished);
                }

                function jsonToCSV(json) {
                    Papa.unparse(data)
                        .then(handleParseResult)
                        .catch(handleParseError)
                        .finally(parsingFinished);
                }

                angular.extend(this, {
                    parseCSV: parseCSV,
                    jsonToCSV: jsonToCSV
                });

            }
        })

})();