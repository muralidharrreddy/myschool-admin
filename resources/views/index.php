<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>MySchool24x7 DashBoard-Welcome</title>
    <!-- Bootstrap -->
    <link href='fonts/Ubuntu-Bold.ttf' type='text/css'>
    <link href='fonts/Ubuntu-Light.ttf' type='text/css'>
    <link href='fonts/Ubuntu-Medium.ttf'  type='text/css'>
    <link href='fonts/Ubuntu-Regular.ttf' type='text/css'>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/index.css" rel="stylesheet">
    <link href="css/news.css" rel="stylesheet">
    <link href="css/events.css" rel="stylesheet">
    <link href="css/results.css" rel="stylesheet">
    <link href="css/attendance.css" rel="stylesheet">
    <link href="css/comment.css" rel="stylesheet">
    <link href="css/welcome.css" rel="stylesheet">
    <link href="css/angular-notify.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="bower_components/ng-img-crop-full-extended/compile/minified/ng-img-crop.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="bower_components/angular-bootstrap/ui-bootstrap-csp.css">
    <link rel="stylesheet" type="text/css" href="bower_components/sweetalert/dist/sweetalert.css">
    <link rel="stylesheet" type="text/css" href="bower_components/angular-block-ui/dist/angular-block-ui.min.css">
    <link rel="stylesheet" type="text/css" href="bower_components/angular-loading-bar/src/loading-bar.css">
    <link rel="stylesheet" type="text/css" href="bower_components/angularjs-datepicker/dist/angular-datepicker.min.css">
    <link rel="stylesheet" type="text/css" href="bower_components/select2/select2.css">
    <link rel="stylesheet" type="text/css" href="bower_components/select2/select2-bootstrap.css">
</head>
<body ng-app="authApp">
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">

            <!-- <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button> -->
            <a class="navbar-brand" href="#"><!-- <span class="glyphicon glyphicon-education navIconMargin"
                                                   aria-hidden="true"></span> -->MySchool24x7</a>

        </div>
        <div class="pull-right">
            <span style="margin-top: 30px;font-size: larger;margin-right: 35px" >{{currentUser.userName}}</span> <button ng-if="isLogin" class="btn btn-danger" style="margin-top: 10px" ng-click="logout()">

                Log out</button>
        </div>

    </div>

</nav>
<div class="container-fluid"  style="margin-top:51px;">
    <div  class="row">
        <div  ng-if="isLogin" class="col-xs-12 col-sm-2 col-md-2 col-lg-2 fixedSidebar" >
            <ul  class="nav sidebar-menu">
                <li class="listBottom">
                    <a   ui-sref="welcome" is-active-nav href="/welcome">
                        <span class="sidebar-title">WELCOME</span>
                        <span class="glyphicon glyphicon-home iconFloat" aria-hidden="true"></span>
                    </a>
                </li>
                <li class="listBottom">
                    <a  ui-sref="news" is-active-nav href="/news" >
                        <span class="sidebar-title">NEWS</span>
                        <span class="glyphicon glyphicon-list-alt iconFloat" aria-hidden="true"></span>
                    </a>
                </li>
                <li class="listBottom">
                    <a ui-sref="events" is-active-nav href="/events">
                        <span class="sidebar-title">EVENTS</span>
                        <span class="glyphicon glyphicon-film iconFloat" aria-hidden="true"></span>
                    </a>
                </li>
                <li class="listBottom">
                    <a ui-sref="results" is-active-nav href="/results">
                        <span class="sidebar-title">RESULTS</span>
                        <span class="glyphicon glyphicon-list iconFloat" aria-hidden="true"></span>
                    </a>
                </li>
                <li class="listBottom">
                    <a ui-sref="attendance" is-active-nav href="/attendance">
                        <span class="sidebar-title">ATTENDANCE</span>
                        <span class="glyphicon glyphicon-font iconFloat" aria-hidden="true"></span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- Content -->
        <div class="col-xs-12 col-sm-9 col-sm-offset-2 col-md-8 col-md-offset-2 col-lg-9 col-lg-offset-2 contentblock">
            <div ui-view></div>
        </div>
        <div  ng-if="authenticated"  class="col-xs-12 col-sm-1 col-sm-offset-11 col-md-2 col-md-offset-10 col-lg-1 col-lg-offset-11 btnBlock" style="">
            <button ng-if="isLogin&&data.showPreviewBtn"   ng-click="showSchoolInfo()" class="btn btn-default formSubmitBtn" type="submit" style="background:#00a9b7;color:#fff">Preview</button>
        </div>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<!-- Footer -->
<!-- <footer class="footerPosition" style="">
    <div class="container-fluid">
        <div class="row">

        </div>
    </div>
</footer> -->
<!-- Large modal -->
<!--   -->

<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" style="background:#fff">
            Data
        </div>
    </div>
</div>



<script src="bower_components/jquery/dist/jquery.min.js"></script>
<script src="bower_components/select2/select2.min.js"></script>
<script src="bower_components/sweetalert/dist/sweetalert.min.js"></script>
<script src="bower_components/tinymce-dist/tinymce.min.js"></script>
<script src="scripts/xml2json.min.js"></script>
<script src="node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="bower_components/papaparse/papaparse.js"></script>
<script src="bower_components/angular/angular.js"></script>
<script src="node_modules/angular-ui-router/build/angular-ui-router.js"></script>
<script src="node_modules/satellizer/satellizer.js"></script>
<script src="bower_components/angular-papa-promise/dist/angular-papa-promise.js"></script>
<script src="bower_components/angular-sanitize/angular-sanitize.min.js"></script>
<script src="bower_components/ng-file-upload/ng-file-upload-all.min.js"></script>
<script src="bower_components/angular-block-ui/dist/angular-block-ui.min.js"></script>
<script src="bower_components/angular-loading-bar/src/loading-bar.js"></script>
<script src="bower_components/angularjs-datepicker/dist/angular-datepicker.min.js"></script>
<script src="bower_components/angular-ui-tinymce/dist/tinymce.min.js"></script>
<script src="scripts/elastic.js"></script>
<script src="scripts/ngAlert.js"></script>
<script src="bower_components/angular-bootstrap/ui-bootstrap-tpls.js"></script>
<script src="bower_components/ng-img-crop-full-extended/compile/minified/ng-img-crop.js"></script>
<script src="scripts/angular-image-compress.js"></script>
<script src="scripts/angular-notify.js"></script>
<script src="bower_components/angular-ui-select2/src/select2.js"></script>
<script src="bower_components/angular-csv-import/dist/angular-csv-import.min.js"></script>
<script src="scripts/app.js"></script>
<script src="scripts/attendanceCtrl.js"></script>
<script src="scripts/authController.js"></script>
<script src="scripts/eventsCtrl.js"></script>
<script src="scripts/newsCtrl.js"></script>
<script src="scripts/resultsCtrl.js"></script>
<script src="scripts/welcomeCtrl.js"></script>
<script src="scripts/profileCtrl.js"></script>
<script src="scripts/userData.js"></script>
<script type="text/ng-template" id="imageUpload.html">
    <form name="myForm" style="margin-left: 25px;">
        <div class="modal-header">
            <h3 class="modal-title" id="modal-title">Select Image</h3>
        </div>
        <div class="modal-body" id="modal-body">
            <input type="file" ngf-select ng-model="picFile" name="file"
                   accept="image/*" ngf-max-size="0.2MB" required
                   ngf-model-invalid="errorFile">
            <i ng-show="myForm.file.$error.required">*required</i><br>
            <i ng-show="myForm.file.$error.maxSize">File too large
                {{errorFile.size / 1000000|number:1}}MB: max 200KB</i>
            <img ng-show="myForm.file.$valid" ngf-thumbnail="picFile" class="thumb">
            <br>
                <span class="progress" ng-show="picFile.progress >= 0">
                <div style="width:{{picFile.progress}}%"
                     ng-bind="picFile.progress + '%'"></div>
              </span>
            <span ng-show="picFile.result">Upload Successful</span>
            <span class="err" ng-show="errorMsg">{{errorMsg}}</span>
        </div>
        <div class="modal-footer">
            <button class="btn btn-primary" ng-disabled="!myForm.$valid"
                    ng-click="submitImage(picFile)" type="submit">Upload</button>

            <button class="btn btn-warning" type="button" ng-click="closeModal()">Cancel</button>
        </div>
    </form>
</script>

</body>
</html>