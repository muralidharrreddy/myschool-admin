<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('index');
});

Route::group(['prefix' => 'api'], function()
{
	Route::resource('authenticate', 'AuthenticateController', ['only' => ['index']]);
	Route::resource('schoolData', 'SchoolController', ['only' => ['index']]);
	Route::resource('attendanceData', 'AttendanceController', ['only' => ['index']]);
	Route::resource('getResourceData', 'ResourceController', ['only' => ['index']]);
	Route::post('authenticate', 'AuthenticateController@authenticate');
	Route::get('authenticate/user', 'AuthenticateController@getAuthenticatedUser');
	Route::get('authenticate/logout', 'AuthenticateController@logout');
	Route::get('schoolData/welcome', 'SchoolController@getSchoolData');
	Route::get('schoolData/news', 'SchoolController@getnewsData');
	Route::post('schoolData/news', 'SchoolController@postnewsData');
	Route::put('schoolData/news', 'SchoolController@putnewsData');
	Route::get('schoolData/getEventsData', 'SchoolController@getEventsData');
	Route::post('schoolData/getEventsData', 'SchoolController@postEventsData');
	Route::put('schoolData/getEventsData', 'SchoolController@putEventsData');
	Route::get('attendanceData/getAttendanceData', 'AttendanceController@getAttendanceData');
	Route::put('attendanceData/putAttendanceData', 'AttendanceController@putAttendanceData');
	Route::delete('attendanceData/deleteAttendanceDataByUID', 'AttendanceController@deleteAttendanceDataByUID');
	Route::get('attendanceData/getAttendanceDataByUID', 'AttendanceController@getAttendanceDataByUID');
	Route::post('attendanceData/postAttendanceData', 'AttendanceController@postAttendanceData');
	//Route::post('schoolData/welcome', 'SchoolController@postSchoolData');
	Route::put('schoolData/welcome', 'SchoolController@putSchoolData');
	Route::get('getResourceData/getS3policyPlain', 'ResourceController@getS3policyPlain');
	Route::get('getResourceData/getNotifications', 'ResourceController@getNotifications');
	Route::post('getResourceData/sendNotification', 'ResourceController@sendNotification');
	Route::post('getResourceData/sendNotificationToSegment', 'ResourceController@sendNotificationToSegment');
});
